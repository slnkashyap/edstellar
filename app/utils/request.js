import { create } from 'apisauce'
export const BASE_URL = process.env.API_URL ? process.env.API_URL : 'http://localhost:3000/api/v1/';

export class Api {

    constructor(baseURL) {
      this.apiSauce = create({
        baseURL,
        headers: {
          "Cache-Control": "no-cache",
        },
        timeout: 60 * 1000,
      });
    }

    _get = async (url, payload) => {
      const res = await this._handleResponse(this.apiSauce.get, { url, payload });
      return res;
    };

    _put = async (url, payload) => {
      const res = await this._handleResponse(this.apiSauce.put, { url, payload });
      return res;
    };

    _post = async (url, payload) => {
      const res = await this._handleResponse(this.apiSauce.post, { url, payload });
      return res;
    };

    _delete = async (url, payload) => {
      const res = await this._handleResponse(this.apiSauce.delete, { url, payload });
      return res;
    };


    _handleResponse = async (apiRequest, params) => {
      const Authorization = localStorage.getItem('token');
       const res = await apiRequest(params.url, params.payload, { headers: { Authorization }});
       return this._handleError(res);
    };

    _handleError = (res) => {
      if (!res.ok) {
        if (res.status === 401 || res.status === 403) {
          // **redirect to login page**
        //   const error = new Error(res.problem);
        //   error.response = res;
        //   throw error;
        }
        if (res.data && res.data.message) {
          // showNotification(res.data.message);
        }
        // showNotification(res.problem);
      }
      return res.data;
    };
}

const MyApi = new Api(BASE_URL);

export default MyApi;