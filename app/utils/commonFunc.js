/*
  utility functions used accross app
*/
export const splitBySpace = (string) => string.split(/(\s+)/);
export const splitByDot = (string) => string.split(/(\.+)/);
export const splitByUnderscore = (string) => string.split(/(_+)/);
export const splitByColon = (string) => string.split(/(:+)/);
export const stringEncodeObject = (data) => Object.keys(data).map((key) => `${key}=${encodeURIComponent(data[key])}`).join('&');
export const readLocalKey = (key) => localStorage.getItem(key);
export const setLocalKey = (key) => (token) => localStorage.setItem(key, token);
export const removeLocalKey = (key) => localStorage.removeItem(key);
export const removeAllLocalKeys = () => localStorage.clear();