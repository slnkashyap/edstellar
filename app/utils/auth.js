import { browserHistory } from 'react-router';
import request from '../utils/request';
const localStorage = global.window.localStorage;

const auth = {
  login(payload) {
    // if (auth.loggedIn()) {
    //   return Promise.resolve(true);
    // }

    return request._post('/authenticate', payload)
      .then((response) => response)
      .catch(error => {
        console.error(error);
      });
  },

  loggedIn() {
    return !!localStorage.token;
  },

  signup(username, password) {
    // // Post a fake request
    // return request.post('/signup', { username, password })
    //   // Log user in after registering
    //   // json-server does not refresh automatically, so in dev I just reroute to login
    //   // and add a success message
    //   .then(() => {
    //     forwardTo('/login?success');
    //     // auth.login(username, password); // reroute
    //   });
  },
};

function forwardTo(location) {
  browserHistory.push(location);
}

export default auth;