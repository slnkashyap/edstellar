/**
 * App sagas
 */
import { LOCATION_CHANGE, push } from 'react-router-redux';
import { call, cancel, select, take, takeLatest, put, race } from 'redux-saga/effects';
import {  isUserLoggedIn } from 'containers/Auth/selectors';
// import * as errorMessages from './errorMessages';
import request from '../../utils/request';
import userService from './service';



import {
  saveUsersList,
} from './actions';

import { GET_USERS_LIST } from './constants';

export function* getUsersSaga() {

  try {
      const response = yield call(userService.getAllUsers, {});
      yield put(saveUsersList(response));
  } catch (e) {
    console.log(e);
    yield put(saveUsersList({ data: [] }));
  }
}

export default function* rootSaga() {
  const userSagaWatcher = yield takeLatest(GET_USERS_LIST, getUsersSaga);
  // yield take(LOCATION_CHANGE);
  // yield cancel(userSagaWatcher);
}