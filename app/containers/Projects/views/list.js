/**
 *
 * User
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import makeSelectUser, { usersListSelector, listLoadingSelector } from '../selectors';
import { getUsersList } from '../actions'
import { useInjectSaga } from 'utils/injectSaga';
import saga from '../saga';


export function UserList(props) {
  const { dispatch, usersList, is_loading } = props;
  useInjectSaga({ key: 'project', saga });
  useEffect(() => {
    dispatch(getUsersList())
  }, []);
  return (
    <div>
      <Helmet>
        <title>User</title>
        <meta name="description" content="Description of User" />
      </Helmet>
      <div className="row layout-top-spacing">
        <div className="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
          <Link to="/project/create"><button className="btn btn-primary mb-2">Add a Project</button></Link>

          <div className="statbox widget box box-shadow">
            {
              is_loading ? (
                <div className="d-flex justify-content-between mx-5 mt-3 mb-5">
                  <div className="spinner-border spinner-border-reverse align-self-center loader-sm text-secondary">Loading...</div>
                </div>) : (<div className="table-responsive">
                  {
                    usersList.length ? (<table className="table table-bordered mb-4">
                      <thead>
                        <tr>
                          <th>Sl No</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          usersList.map(({ user }, idx) => (
                            <tr key={user.id}>
                              <td>{idx + 1}</td>
                              <td>{user.name}</td>
                              <td>{user.email}</td>
                              <td>{user.phone}</td>
                            </tr>
                          ))
                        }
                      </tbody>
                    </table>
                    ) : null
                  }
                </div>)
            }
          </div>
        </div>
      </div>
    </div>
  );
}

UserList.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
  usersList: usersListSelector(),
  is_loading: listLoadingSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getUsersList
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(UserList);
