/**
 *
 * User
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import makeSelectUser, { usersListSelector, listLoadingSelector } from '../selectors';
import { getUsersList } from '../actions'
import { useInjectSaga } from 'utils/injectSaga';
import saga from '../saga';


export function CreateProject(props) {
    const { dispatch, usersList, is_loading } = props;
    useInjectSaga({ key: 'project', saga });
    return (
        <div>
            <Helmet>
                <title>User</title>
                <meta name="description" content="Description of User" />
            </Helmet>
            <div className="row">
                <div id="flFormsGrid" className="col-8 layout-spacing mt-4">
                    <div className="statbox widget box box-shadow">
                        <div className="widget-header">
                            <div className="row">
                                <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>Create a New Project</h4>
                                </div>
                            </div>
                        </div>
                        <div className="widget-content widget-content-area">
                            <form>
                                <div className="form-row mb-4">
                                    <div className="form-group col-md-12">
                                        <label htmlFor="inputEmail4">Title</label>
                                        <input
                                            className="form-control"
                                            placeholder="Project title"
                                        />
                                    </div>
                                </div>
                                <div className="form-group mb-4">
                                    <label htmlFor="inputAddress">Description</label>
                                    <textarea
                                        type="text"
                                        className="form-control"
                                        id="inputAddress"
                                        placeholder="Enter Project Description"
                                        style={{
                                            height: 'auto'
                                        }}
                                    />
                                </div>
                                {/* <div className="form-row mb-4">
                                    <div className="form-group col-md-6">
                                        <label htmlFor="inputCity">City</label>
                                        <input type="text" className="form-control" id="inputCity" />
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label htmlFor="inputState">State</label>
                                        <select id="inputState" className="form-control">
                                            <option selected>Choose...</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div className="form-group col-md-2">
                                        <label htmlFor="inputZip">Zip</label>
                                        <input type="text" className="form-control" id="inputZip" />
                                    </div>
                                </div> */}

                                <button type="submit" className="btn btn-primary mt-3">
                                    Next Step
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

CreateProject.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
    user: makeSelectUser(),
    usersList: usersListSelector(),
    is_loading: listLoadingSelector(),
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        getUsersList
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(withConnect)(CreateProject);
