/*
 *
 * User constants
 *
 */

export const GET_USERS_LIST = 'app/PROJECT/GET_USERS_LIST';
export const ON_USERS_LIST_GET_SUCCESS = 'app/User/ON_PROJECT_LIST_GET_SUCCESS';
export const ON_USERS_LIST_GET_FAIL = 'app/User/ON_PROJECT_LIST_GET_FAIL';
