/*
 *
 * User actions
 *
 */

import { GET_USERS_LIST, ON_USERS_LIST_GET_SUCCESS } from './constants';

export function getUsersList() {
  return {
    type: GET_USERS_LIST,
  };
}

export function saveUsersList(users) {
  return {
    type: ON_USERS_LIST_GET_SUCCESS,
    users,
  }
}