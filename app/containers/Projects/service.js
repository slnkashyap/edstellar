
import request from '../../utils/request';
const userService = {
  getAllUsers(payload, headers) {
    return request._get('/projects', payload, headers)
      .then((response) => response)
      .catch(error => {
        console.error(error);
      });
  },
};

export default userService;