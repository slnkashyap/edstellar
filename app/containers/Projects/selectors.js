import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the user state domain
 */

const selectUserDomain = state => state.project || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by User
 */

const makeSelectUser = () =>
  createSelector(
    selectUserDomain,
    substate => substate,
  );

const usersListSelector = () =>
  createSelector(
    selectUserDomain,
    substate => substate.items,
  );  

const listLoadingSelector = () =>
  createSelector(
    selectUserDomain,
    substate => substate.is_fetching_list,
  );  

export default makeSelectUser;
export { selectUserDomain, usersListSelector, listLoadingSelector };
