/**
 *
 * LoginPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLoginPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import background_image from '../../images/login.jpg';
import LoginForm from 'components/LoginForm/Loadable';
import { onLoginAttempt } from 'containers/Auth/actions';

export function LoginPage(props) {
  // useInjectReducer({ key: 'loginPage', reducer });
  // useInjectSaga({ key: 'loginPage', saga });
  const { dispatch } = props;
  return (
    <div>
      <Helmet>
        <title>LoginPage</title>
        <meta name="description" content="Description of LoginPage" />
      </Helmet>
      <div className="form">
        <div className="form-container">
          <div className="form-form">
            <div className="form-form-wrap">
              <div className="form-container">
                <div className="form-content">
                  <h1>
                    Log In to{" "}
                    <a href="index.html">
                      <span className="brand-name">Edsteller</span>
                    </a>
                  </h1>
                  <p className="signup-link">
                    New Here? <a href="auth_register.html">Create an account</a>
                  </p>
                  <LoginForm onSubmit={(values) => dispatch(onLoginAttempt(values))} />
                  <p className="terms-conditions">
                    © 2020 All Rights Reserved. <a href="index.html">Edsteller</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="form-image">
            <div className="l-image" style={{ background: `url(${background_image})`, backgroundSize: 'cover' }} />
          </div>
        </div>
      </div>
    </div>
  );
}

LoginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginPage: makeSelectLoginPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(LoginPage);
