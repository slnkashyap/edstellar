/*
 *
 * LoginPage reducer
 *
 */
import produce from 'immer';
import { ON_LOGIN_ATTEMPT } from './constants';

export const initialState = {
  is_attempting_login: null,
};

/* eslint-disable default-case, no-param-reassign */
const loginPageReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case ON_LOGIN_ATTEMPT:
          draft.is_attempting_login = true;
        break;
    }
  });

export default loginPageReducer;
