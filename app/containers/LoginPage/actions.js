/*
 *
 * LoginPage actions
 *
 */

import { ON_LOGIN_ATTEMPT } from './constants';

export function onLoginAttempt(data) {
  return {
    type: ON_LOGIN_ATTEMPT,
    data
  };
}
