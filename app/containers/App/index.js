/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import AuthenticationFlow from 'containers/Auth/Loadable';
import MainApp from 'containers/Root/Loadable';
import GlobalStyle from '../../global-styles';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { isUserLoggedIn } from '../Auth/selectors';

export function App(props) {
  const { is_loggedin } = props;
  useEffect(() => {
    if (!props.isUserLoggedIn) {
      props.dispatch(push('/login'))
    }
  }, []);
  return (
    <div>
      {
        is_loggedin ? <MainApp/> : <AuthenticationFlow />
      }
      <GlobalStyle />
    </div>
  );
}


const mapStateToProps = createStructuredSelector({
  is_loggedin: isUserLoggedIn(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);


export default compose(withConnect)(App);