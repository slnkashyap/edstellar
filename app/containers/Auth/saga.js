/**
 * App sagas
 */
import { browserHistory } from 'react-router';
import { LOCATION_CHANGE, push } from 'react-router-redux';
import { call, cancel, select, take, takeLatest, put, race } from 'redux-saga/effects';
import auth from '../../utils/auth';
// import * as errorMessages from './errorMessages';

const localStorage = global.window.localStorage;

import {
  sendingRequest,
  setAuthState,
  // setErrorMessage,
} from './actions';

import { LOGIN_ATTEMPT } from './constants';

export function* authorize({ newUser, payload }) {
  yield put(sendingRequest(true));
  try {
    let response;
    if (newUser) {
      // response = yield call(auth.signup, username, hash);
    } else {
      response = yield call(auth.login, payload);
    }
    return response;
  } catch (error) {
    // yield put(setErrorMessage(error.message));
    return false;
  } finally {
    yield put(sendingRequest(false));
  }
}

export function* login({ data }) {
  const newUser = false;
  const winner = yield race({
    auth: call(authorize, { newUser, payload: data }),
    // logout: take(LOGOUT),
  });
  if (winner.auth) {
    yield put(setAuthState(winner.auth.auth_token));
    localStorage.token = winner.auth.auth_token;
    // forwardTo('/dashboard');
  }
}

// export function* signup() {
//   const userDetails = yield select(makeSelectFormState());
//   const username = userDetails.get('username');
//   const password = userDetails.get('password');
//   const newUser = true;

//   const response = yield call(authorize, { newUser, username, password });
//   console.log(response);

//   if (response) {
//     yield put(setAuthState(true));
//     yield put(changeForm('', ''));
//     forwardTo('/dashboard');
//   }
// }


function forwardTo(location) {
  browserHistory.push(location);
}


// export function* userSignup() {
//   const watcher = yield takeLatest(SIGNUP, signup);
//   yield take(LOCATION_CHANGE);
//   yield cancel(watcher);
// }


export default function* rootSaga() {
  const loginSagaWatacher = yield takeLatest(LOGIN_ATTEMPT, login);
  yield take(LOCATION_CHANGE);
  yield cancel(loginSagaWatacher);
}