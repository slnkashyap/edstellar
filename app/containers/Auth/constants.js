/*
 *
 * Auth constants
 *
 */

export const LOGIN_ATTEMPT = 'app/Auth/LOGIN_ATTEMPT';
export const LOGIN_SUCESS = 'app/Auth/LOGIN_SUCESS';
export const LOGIN_FAIL = 'app/Auth/LOGIN_FAIL';

export const SENDING_REQUEST = 'app/Auth/SENDING_REQUEST';
export const SET_AUTH = 'app/Auth/SET_AUTH';