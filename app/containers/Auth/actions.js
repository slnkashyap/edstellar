/*
 *
 * Auth actions
 *
 */

import { LOGIN_ATTEMPT, SENDING_REQUEST, SET_AUTH } from './constants';

export function onLoginAttempt(data) {
  return {
    type: LOGIN_ATTEMPT,
    data
  };
}

export function sendingRequest(sending) {
  return {
    type: SENDING_REQUEST,
    sending,
  };
}

export function setAuthState(newToken) {
  return {
    type: SET_AUTH,
    newToken,
  };
}
