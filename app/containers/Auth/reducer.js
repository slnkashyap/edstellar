/*
 *
 * Auth reducer
 *
 */
import produce from 'immer';
import { LOGIN_ATTEMPT, SENDING_REQUEST, SET_AUTH } from './constants';
import auth from '../../utils/auth';

export const initialState = {
  currentlySending: false,
  loggedIn: auth.loggedIn(),
  errorMessage: '',
  tokenValue: localStorage.getItem('token')
};

/* eslint-disable default-case, no-param-reassign */
const authReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SENDING_REQUEST:
          draft.currentlySending = action.sending;
        break;
      case SET_AUTH:
        draft.tokenValue = action.newToken
      break  
    }
  });

export default authReducer;
