import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the auth state domain
 */

const selectAuthDomain = state => state.auth || initialState;

const makeSelectAuth = () =>
  createSelector(
    selectAuthDomain,
    substate => substate,
  );

const isUserLoggedIn = () => createSelector(
  selectAuthDomain,
  substate => substate.tokenValue
);
  
export default makeSelectAuth;
export { selectAuthDomain, isUserLoggedIn };
