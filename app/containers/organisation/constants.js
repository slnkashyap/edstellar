/*
 *
 * User constants
 *
 */

export const GET_USERS_LIST = 'app/User/GET_ORG_LIST';
export const ON_USERS_LIST_GET_SUCCESS = 'app/User/ON_ORG_LIST_GET_SUCCESS';
export const ON_USERS_LIST_GET_FAIL = 'app/User/ON_ORG_LIST_GET_FAIL';
