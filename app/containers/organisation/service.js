
import request from '../../utils/request';
const userService = {
  getAllUsers(payload) {
    return request._get('/organisations', payload)
      .then((response) => response)
      .catch(error => {
        console.error(error);
      });
  },
};

export default userService;