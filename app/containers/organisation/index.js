/**
 *
 * User
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Switch, Route } from 'react-router-dom';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectUser, { usersListSelector } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { getUsersList } from './actions'
import UserList from './views/list';

export function User(props) {
  useInjectReducer({ key: 'org', reducer });
  useInjectSaga({ key: 'org', saga });
  return (
    <div>
      <Helmet>
        <title>User</title>
        <meta name="description" content="Description of User" />
      </Helmet>
      <Switch>
        <Route exact path="/org" component={UserList} />
      </Switch>
    </div>
  );
}

User.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
  usersList: usersListSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getUsersList
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(User);
