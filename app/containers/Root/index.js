/**
 *
 * Root
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Switch, Route } from 'react-router-dom';
import makeSelectRoot from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import UserPage from 'containers/User/Loadable';
import OrganisationPage from 'containers/organisation/Loadable';
import ProjectPage from 'containers/Projects/Loadable';
import Header from 'components/Header/Loadable';
import Sidebar from 'components/Sidebar/Loadable';

export function Root() {
  useInjectReducer({ key: 'root', reducer });
  useInjectSaga({ key: 'root', saga });

  return (
    <div>
      <Helmet>
        <title>Root</title>
        <meta name="description" content="Description of Root" />
      </Helmet>
      <Header />
      <div className="main-container" id="container">
        <Sidebar />
        <div id="content" className="main-content">

          <div className="layout-px-spacing">
            <Switch>
              <Route path="/user" component={UserPage} />
              <Route  path="/org" component={OrganisationPage} />
              <Route  path="/project" component={ProjectPage} />
              <Route component={UserPage} />
            </Switch>
          </div>
        </div>
      </div>

    </div>
  );
}

Root.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  root: makeSelectRoot(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Root);
