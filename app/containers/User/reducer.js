/*
 *
 * User reducer
 *
 */
import produce from 'immer';
import { GET_USERS_LIST, ON_USERS_LIST_GET_SUCCESS } from './constants';

export const initialState = {
  is_fetching_list: false,
  items: [],
};

/* eslint-disable default-case, no-param-reassign */
const userReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_USERS_LIST:
          draft.is_fetching_list = true;
        break;
      case ON_USERS_LIST_GET_SUCCESS: {
        return {...draft, items: action.users, is_fetching_list: false}
      }
    }
  });

export default userReducer;
