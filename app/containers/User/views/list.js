/**
 *
 * User
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import makeSelectUser, { usersListSelector } from '../selectors';
import { getUsersList } from '../actions'
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from '../reducer';
import saga from '../saga';


export function UserList(props) {
  const { dispatch, usersList } = props;
  useInjectSaga({ key: 'user', saga });
  useEffect(() => {
    dispatch(getUsersList())
  }, []);
  return (
    <div>
      <Helmet>
        <title>User</title>
        <meta name="description" content="Description of User" />
      </Helmet>
      <div className="row layout-top-spacing">
        <div className="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        <div className="statbox widget box box-shadow">
          <div className="table-responsive">
            <table className="table table-bordered mb-4">
              <thead>
                <tr>
                <th>Sl No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                </tr>
              </thead>
              <tbody>
                {
                  usersList && usersList.map(({ user }, idx) => (
                    <tr key={user.id}>
                      <td>{idx+1}</td>
                      <td>{user.name}</td>
                      <td>{user.email}</td>
                      <td>{user.phone}</td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          </div>
        </div>

        </div>
      
      </div>
    </div>
  );
}

UserList.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
  usersList: usersListSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getUsersList
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(UserList);
