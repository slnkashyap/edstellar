
import request from '../../utils/request';
const userService = {
  getAllUsers(payload) {
    return request._get('/users', payload)
      .then((response) => response)
      .catch(error => {
        console.error(error);
      });
  },
};

export default userService;