/*
 *
 * User constants
 *
 */

export const GET_USERS_LIST = 'app/User/GET_USERS_LIST';
export const ON_USERS_LIST_GET_SUCCESS = 'app/User/ON_USERS_LIST_GET_SUCCESS';
export const ON_USERS_LIST_GET_FAIL = 'app/User/ON_USERS_LIST_GET_FAIL';
