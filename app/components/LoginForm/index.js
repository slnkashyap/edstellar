/**
 *
 * LoginForm
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { Formik } from "formik";
import * as Yup from "yup";

const validation_schema = Yup.object().shape({
  email: Yup.string()
    .email()
    .required("Required"),
  password: Yup.string()
    .required("Required")  
})
const initial_values = { email: "", password: "" };
function LoginForm(props) {
  const { onSubmit } = props;
  return (
    <Formik
      validateOnChange={false}
      initialValues={initial_values}
      onSubmit={(values)=> onSubmit(values)}
      validationSchema={validation_schema}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          dirty,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset
        } = props;
        return (
          <form className="text-left" onSubmit={(event)=> {event.preventDefault(); handleSubmit()}}>
          <div className="form">
            <div id="username-field" className="field-wrapper input">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-user"
              >
                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
                <circle cx={12} cy={7} r={4} />
              </svg>
              <input
                name="email"
                type="text"
                className="form-control"
                placeholder="Email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <div className="invalid-feedback" style={{ display: errors.email && touched.email}}>
               {errors.email}
            </div>
            </div>
            <div id="password-field" className="field-wrapper input mb-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-lock"
              >
                <rect x={3} y={11} width={18} height={11} rx={2} ry={2} />
                <path d="M7 11V7a5 5 0 0 1 10 0v4" />
              </svg>
              <input
                id="password"
                name="password"
                type="password"
                className="form-control"
                placeholder="Password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              <div className="invalid-feedback" style={{ display: errors.password && touched.password}}>
               {errors.password}
            </div>
            </div>
            <div className="d-sm-flex justify-content-between">
              <div className="field-wrapper toggle-pass">
                <p className="d-inline-block">Show Password</p>
                <label className="switch s-primary">
                  <input
                    type="checkbox"
                    id="toggle-password"
                    className="d-none"
                  />
                  <span className="slider round" />
                </label>
              </div>
              <div className="field-wrapper">
                <button type="submit" className="btn btn-primary" value>
                  Log In
                </button>
              </div>
            </div>
            <div className="field-wrapper text-center keep-logged-in">
              <div className="n-chk new-checkbox checkbox-outline-primary">
                <label className="new-control new-checkbox checkbox-outline-primary">
                  <input type="checkbox" className="new-control-input" />
                  <span className="new-control-indicator" />
                  Keep me logged in
                </label>
              </div>
            </div>
            <div className="field-wrapper">
              <a
                href="auth_pass_recovery.html"
                className="forgot-pass-link"
              >
                Forgot Password?
              </a>
            </div>
          </div>
        </form>
       );
      }}
    </Formik>

  );
}

LoginForm.propTypes = {};

export default LoginForm;
