/**
 *
 * Header
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function Header() {
  return (
    <div>
    <div className="header-container fixed-top">
  <header className="header navbar navbar-expand-sm">
    <ul className="navbar-item theme-brand flex-row  text-center">
      <li className="nav-item theme-logo">
        <a href="index.html">
          {/* <img src="assets/img/90x90.jpg" className="navbar-logo" alt="logo" /> */}
        </a>
      </li>
      <li className="nav-item theme-text">
        <a href="index.html" className="nav-link">
          {" "}
          Edstellar{" "}
        </a>
      </li>
    </ul>
    <ul className="navbar-item flex-row ml-md-0 ml-auto">
      <li className="nav-item align-self-center search-animated">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
          fill="none"
          stroke="currentColor"
          strokeWidth={2}
          strokeLinecap="round"
          strokeLinejoin="round"
          className="feather feather-search toggle-search"
        >
          <circle cx={11} cy={11} r={8} />
          <line x1={21} y1={21} x2="16.65" y2="16.65" />
        </svg>
        <form
          className="form-inline search-full form-inline search"
          role="search"
        >
          <div className="search-bar">
            <input
              type="text"
              className="form-control search-form-control  ml-lg-auto"
              placeholder="Search..."
            />
          </div>
        </form>
      </li>
    </ul>
  </header>
</div>
<div className="sub-header-container">
  <header className="header navbar navbar-expand-sm">
    <a
      href="javascript:void(0);"
      className="sidebarCollapse"
      data-placement="bottom"
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width={24}
        height={24}
        viewBox="0 0 24 24"
        fill="none"
        stroke="currentColor"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
        className="feather feather-menu"
      >
        <line x1={3} y1={12} x2={21} y2={12} />
        <line x1={3} y1={6} x2={21} y2={6} />
        <line x1={3} y1={18} x2={21} y2={18} />
      </svg>
    </a>
    <ul className="navbar-nav flex-row">
      <li>
        <div className="page-header">
          <nav className="breadcrumb-one" aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="javascript:void(0);">Subheader Add Breadcrumbs</a>
              </li>

            </ol>
          </nav>
        </div>
      </li>
    </ul>
  </header>
</div>
</div>

  );
}

Header.propTypes = {};

export default Header;
