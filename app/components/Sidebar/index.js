/**
 *
 * Sidebar
 *
 */

import React from 'react';
import { NavLink } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Sidebar() {
  return (
    <div className="sidebar-wrapper sidebar-theme">
      <nav id="sidebar">
        <div className="shadow-bottom " />
        <ul className="list-unstyled menu-categories ps" id="accordionExample">
          <li className="menu">
            <NavLink
              to="/user"
              data-toggle="collapse"
              aria-expanded="false"
              className="dropdown-toggle"
            >
              <div>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth={2}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-terminal"
                >
                  <polyline points="4 17 10 11 4 5" />
                  <line x1={12} y1={19} x2={20} y2={19} />
                </svg>
                <span>Users</span>
              </div>
            </NavLink>
            <ul
              className="collapse submenu list-unstyled"
              id="starter-kit"
              data-parent="#accordionExample"
            >
              <li>
                <a href="starter_kit_blank_page.html"> Blank Page </a>
              </li>
              <li>
                <a href="starter_kit_breadcrumbs.html"> Breadcrumbs </a>
              </li>
              <li>
                <a href="starter_kit_boxed.html"> Boxed </a>
              </li>
              <li>
                <a href="starter_kit_alt_menu.html"> Alternate Menu </a>
              </li>
            </ul>
          </li>
          <li className="menu">
            <NavLink
              to="/org"
              data-toggle="collapse"
              aria-expanded="false"
              className="dropdown-toggle"
            >
              <div>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth={2}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-terminal"
                >
                  <polyline points="4 17 10 11 4 5" />
                  <line x1={12} y1={19} x2={20} y2={19} />
                </svg>
                <span>Organisation</span>
              </div>
            </NavLink>
          </li>
          <li className="menu">
            <NavLink
              to="/project"
              className="dropdown-toggle"
            >
              <div>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth={2}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-terminal"
                >
                  <polyline points="4 17 10 11 4 5" />
                  <line x1={12} y1={19} x2={20} y2={19} />
                </svg>
                <span>Projects</span>
              </div>
            </NavLink>
          </li>
          <li className="menu">
            <a
              onClick={ () => { window.localStorage.removeItem('token');  window.location.reload();}}
              className="dropdown-toggle"
            >
              <div>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth={2}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-terminal"
                >
                  <polyline points="4 17 10 11 4 5" />
                  <line x1={12} y1={19} x2={20} y2={19} />
                </svg>
                <span>Logout</span>
              </div>
            </a>
          </li>
        </ul>
      </nav>
    </div>

  );
}

Sidebar.propTypes = {};

export default Sidebar;
