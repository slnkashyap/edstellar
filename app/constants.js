export const API_SYNC_INTERVAL = 300000; // 5 mins between polling for api requests
export const API_SYNC_INTERVAL_SMALL = 120000; // 2 mins between polling for api requests

export const API_VERSION = process.env.API_URL
  ? `${process.env.API_URL}/api/`
  : 'api/v1';
// export const BASE_URL = process.env.API_URL ? process.env.API_URL : 'https://api.orangeselfstorage.com';
export const BASE_URL = process.env.API_URL
  ? process.env.API_URL
  : 'http://localhost:3000';

export const API_STATUS = {
  initiated: 0,
  success: 1,
  failed: 2,
  error: 3,
};
